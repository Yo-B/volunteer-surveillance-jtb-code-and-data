# Volunteer surveillance computations and plot

Code and data for the companion paper: *The value of volunteer surveillance for the early detection of biological invaders.*

submitted to the **Journal of Theoretical Biology**

Extract the result grids from the zip file in the outputs/ folder to be able to run the plot segments of the script. Conversely, those grids can be generated instead of extracted, but the computations (first half of the script) may take several days to complete depending on the number of cores allocated to the task.

contact: yoann.bourhis@rothamsted.ac.uk and frank.vandenbosch@curtin.edu.au
